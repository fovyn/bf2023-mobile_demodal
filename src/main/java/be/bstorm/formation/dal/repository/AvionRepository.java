package be.bstorm.formation.dal.repository;

import be.bstorm.formation.dal.entity.Avion;
import org.hibernate.Remove;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AvionRepository extends JpaRepository<Avion, Integer>, Specification<Avion> {
    Optional<Avion> findByName(String name);

    @Query(value = "DELETE FROM Avion a WHERE a.name = :name")
    void removeByName(@Param("name") String name);

    List<Avion> findAll(Specification<Avion> specification);
}
