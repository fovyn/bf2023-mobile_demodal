package be.bstorm.formation.dal.controllers;

import be.bstorm.formation.dal.entity.Avion;
import be.bstorm.formation.dal.models.AvionCreateForm;
import be.bstorm.formation.dal.models.AvionUpdateForm;
import be.bstorm.formation.dal.repository.AvionRepository;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class AvionController {

    private final AvionRepository avionRepository;


    public AvionController(AvionRepository avionRepository) {
        this.avionRepository = avionRepository;
    }

    @GetMapping(path ="/planeList")
    public Collection<Avion> getAll(){
        return  avionRepository.findAll();

    }

    @GetMapping(path = {"/plane/{name}"})
    public Avion getOneAction(
            @PathVariable String name
    ) {
        return this.avionRepository
                .findByName(name)
                .orElseThrow();
    }

    @PostMapping(path = "/planes")
    public Avion createAction(
            @Valid @RequestBody AvionCreateForm form
    ) {
        Avion avion = form.toEntity();

        this.avionRepository.save(avion);

        return avion;
    }

    @PutMapping(path = {"/planes/{name}"})
    public Avion putAction(
            @PathVariable String name,
            @Valid @RequestBody AvionUpdateForm form
    ) {
        Avion avion = form.toEntity();

        Avion toUpdate = this.avionRepository.findByName(name).orElseThrow();

        toUpdate.setName(avion.getName());

        this.avionRepository.save(toUpdate);

        return toUpdate;
    }
}
