package be.bstorm.formation.dal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bfmobile2023SimpleDalApplication {

    public static void main(String[] args) {
        SpringApplication.run(Bfmobile2023SimpleDalApplication.class, args);
    }

}
