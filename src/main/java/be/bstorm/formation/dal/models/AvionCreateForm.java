package be.bstorm.formation.dal.models;

import be.bstorm.formation.dal.entity.Avion;
import jakarta.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

public record AvionCreateForm(
        @NotBlank
        @Length(max = 10)
        String name
) {

    public Avion toEntity() {
        Avion avion = new Avion();

        avion.setName(name);

        return avion;
    }
}
